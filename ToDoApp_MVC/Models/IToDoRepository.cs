﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ToDoApp.DomainLibrary;

namespace ToDoApp_MVC.Models
{
	public interface IToDoRepository
	{
		public IQueryable<ToDoList> GetToDoLists();
		public IQueryable<ToDoList> GetUserToDoLists(ClaimsPrincipal principal);
		public ToDoList GetToDoListById(int id);
		public Task<ToDoList> GetUserToDoListByIdAsync(ClaimsPrincipal principal, int id, bool includeEntries = false);
		public Task AddAsync(ToDoList toDoList);
		public void Update(ToDoList toDoList);
		public Task UpdateAsync(ToDoList toDoList);
		public Task DeleteAsync(ToDoList toDoList);

		public IQueryable<ToDoEntry> GetToDoEntries();
		public IQueryable<ToDoEntry> GetUserToDoEntries(ClaimsPrincipal principal);
		public ToDoEntry GetToDoEntryById(int id);
		public Task<ToDoEntry> GetUserToDoEntryByIdAsync(ClaimsPrincipal principal, int id);
		public Task AddAsync(ToDoEntry toDoEntry);
		public Task UpdateAsync(ToDoEntry toDoEntry);
		public Task DeleteAsync(ToDoEntry toDoEntry);

		public Task<ToDoUser> GetUserAsync(ClaimsPrincipal principal);
		public string GetUserId(ClaimsPrincipal principal);

		public Task SaveChangesAsync();
	}
}