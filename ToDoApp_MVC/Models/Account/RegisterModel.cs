﻿using System.ComponentModel.DataAnnotations;

namespace ToDoApp_MVC.Models.Account
{
	public class RegisterModel
	{
		[Required]
		public string UserName { get; set; }

		[Required]
		public string FirstName { get; set; }

		[Required]
		public string LastName { get; set; }

		[Required]
		public string Password { get; set; }

		[Required]
		public string PasswordRepeat { get; set; }

		public string ReturnUrl { get; set; } = string.Empty;
	}
}
