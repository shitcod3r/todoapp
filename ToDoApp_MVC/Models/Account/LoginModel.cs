﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ToDoApp_MVC.Models.Account
{
	public class LoginModel
	{
		[Required]
		public string UserName { get; set; }

		[Required]
		public string Password { get; set; }

		public string ReturnUrl { get; set; } = string.Empty;
	}
}
