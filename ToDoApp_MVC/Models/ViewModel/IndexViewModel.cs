﻿using System.Collections.Generic;
using System.Linq;
using ToDoApp.DomainLibrary;

namespace ToDoApp_MVC.Models.ViewModel
{
	public class IndexViewModel
	{
		public IEnumerable<ToDoList> ToDoLists { get; set; } = Enumerable.Empty<ToDoList>();
		public IEnumerable<ToDoList> HiddenLists { get; set; } = Enumerable.Empty<ToDoList>();
		public IEnumerable<ToDoEntry> TasksForToday { get; set; } = Enumerable.Empty<ToDoEntry>();
		public IEnumerable<ToDoEntry> TasksForNext7Days { get; set; } = Enumerable.Empty<ToDoEntry>();
	}
}
