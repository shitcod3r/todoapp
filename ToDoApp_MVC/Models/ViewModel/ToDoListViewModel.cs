﻿using System.ComponentModel.DataAnnotations;

namespace ToDoApp_MVC.Models.ViewModel
{
    public class ToDoListViewModel
    {
        [Required]
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
