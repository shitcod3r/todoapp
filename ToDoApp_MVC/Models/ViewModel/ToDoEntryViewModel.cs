﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoApp_MVC.Models.ViewModel
{
	public class ToDoEntryViewModel
	{
		[Required]
		public string Title { get; set; } = string.Empty;
		public string Description { get; set; } = string.Empty;
		public DateTime DueTime { get; set; }
		public bool HasDueTime { get; set; } = false;
	}
}
