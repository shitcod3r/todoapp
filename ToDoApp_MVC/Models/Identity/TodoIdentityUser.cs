﻿using Microsoft.AspNetCore.Identity;

namespace ToDoApp_MVC.Models.Identity
{
    public class TodoIdentityUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
