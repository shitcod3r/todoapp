﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ToDoApp.DomainLibrary;
using ToDoApp_MVC.Models.Identity;

namespace ToDoApp_MVC.Models
{
	public class ToDoRepository : IToDoRepository
	{
		private readonly TodoDbContext dbContext;
		private readonly UserManager<TodoIdentityUser> userManager;

		public ToDoRepository(TodoDbContext db, UserManager<TodoIdentityUser> usrMngr)
		{
			dbContext = db;
			userManager = usrMngr;
		}

		public async Task AddAsync(ToDoEntry toDoEntry)
		{
			await dbContext.AddAsync(toDoEntry);
			await dbContext.SaveChangesAsync();
		}

		public async Task AddAsync(ToDoList toDoList)
		{
			await dbContext.AddAsync(toDoList);
			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(ToDoEntry toDoEntry)
		{
			dbContext.Remove(toDoEntry);
			await dbContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(ToDoList toDoList)
		{
			dbContext.Remove(toDoList);
			await dbContext.SaveChangesAsync();
		}

		public IQueryable<ToDoEntry> GetToDoEntries()
		{
			throw new System.NotImplementedException();
		}

		public ToDoEntry GetToDoEntryById(int id)
		{
			throw new System.NotImplementedException();
		}

		public ToDoList GetToDoListById(int id)
		{
			throw new System.NotImplementedException();
		}

		public IQueryable<ToDoList> GetToDoLists()
		{
			throw new System.NotImplementedException();
		}

		public async Task<ToDoUser> GetUserAsync(ClaimsPrincipal principal)
		{
			return await dbContext.Users.FirstOrDefaultAsync(u => u.Id == GetUserId(principal));
		}

		public string GetUserId(ClaimsPrincipal principal)
		{
			return userManager.GetUserId(principal);
		}

		public IQueryable<ToDoEntry> GetUserToDoEntries(ClaimsPrincipal principal)
		{
			return GetUserToDoLists(principal).SelectMany(l => l.Entries);
		}

		public async Task<ToDoEntry> GetUserToDoEntryByIdAsync(ClaimsPrincipal principal, int id)
		{
			return await GetUserToDoEntries(principal).Where(t => t.Id == id).FirstOrDefaultAsync();
		}

		public async Task<ToDoList> GetUserToDoListByIdAsync(ClaimsPrincipal principal, int id, bool includeEntries = false)
		{
			System.Linq.Expressions.Expression<Func<ToDoList, bool>> predicate = l => l.Id == id;
			IQueryable<ToDoList> userLists = GetUserToDoLists(principal);

			if (includeEntries)
				return await userLists.Include(l => l.Entries).FirstOrDefaultAsync(predicate);

			return await userLists.FirstOrDefaultAsync(predicate);
		}

		public IQueryable<ToDoList> GetUserToDoLists(ClaimsPrincipal principal)
		{
			return dbContext.ToDoList.Include(l => l.Author).Where(l => l.Author.Id == GetUserId(principal));
		}

		public async Task SaveChangesAsync()
		{
			await dbContext.SaveChangesAsync();
		}

		public async Task UpdateAsync(ToDoEntry toDoEntry)
		{
			dbContext.Update(toDoEntry);
			await dbContext.SaveChangesAsync();
		}

		public void Update(ToDoList toDoList)
		{
			dbContext.Update(toDoList);
		}

		public async Task UpdateAsync(ToDoList toDoList)
		{
			dbContext.Update(toDoList);
			await dbContext.SaveChangesAsync();
		}
	}
}
