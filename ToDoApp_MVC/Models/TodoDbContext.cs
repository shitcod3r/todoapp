﻿using Microsoft.EntityFrameworkCore;
using ToDoApp.DomainLibrary;

namespace ToDoApp_MVC.Models
{
	public class TodoDbContext : DbContext
	{
		public TodoDbContext(DbContextOptions<TodoDbContext> opts) : base(opts) { }

		public DbSet<ToDoList> ToDoList { get; set; }
		public DbSet<ToDoEntry> Tasks { get; set; }
		public DbSet<ToDoUser> Users { get; set; }
	}
}
