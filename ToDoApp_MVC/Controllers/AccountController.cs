﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using ToDoApp.DomainLibrary;
using ToDoApp_MVC.Models;
using ToDoApp_MVC.Models.Account;
using ToDoApp_MVC.Models.Identity;

namespace ToDoApp_MVC.Controllers
{
	[Route("/[controller]/[action]")]
	public class AccountController : Controller
	{
		private readonly ILogger<AccountController> logger;
		private readonly UserManager<TodoIdentityUser> userManager;
		private readonly SignInManager<TodoIdentityUser> signInManager;
		private readonly TodoDbContext dbContext;

		public AccountController(UserManager<TodoIdentityUser> usrMngr, SignInManager<TodoIdentityUser> signInMngr, ILogger<AccountController> lgr, TodoDbContext db)
		{
			userManager = usrMngr;
			signInManager = signInMngr;
			logger = lgr;
			dbContext = db;
		}

		[HttpGet]
		[Route("/[controller]")]
		public IActionResult Index()
		{
			return User.Identity.IsAuthenticated ? RedirectToAction("Profile") : RedirectToAction("Login");
		}

		[HttpGet]
		public IActionResult Register()
		{
			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Profile");
			}

			// TODO: get returnUrl from request and submit within login form
			RegisterModel registerModel = new RegisterModel();
			return View(registerModel);
		}

		[HttpPost]
		public async Task<IActionResult> Register(RegisterModel model)
		{
			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Profile");
			}

			if (ModelState.IsValid)
			{
				TodoIdentityUser user = new TodoIdentityUser()
				{
					Id = Guid.NewGuid().ToString(),
					UserName = model.UserName,
					FirstName = model.FirstName,
					LastName = model.LastName
				};
				IdentityResult result = await userManager.CreateAsync(user, model.Password);
				if (result.Succeeded)
				{
					ToDoUser toDoUser = new ToDoUser()
					{
						Id = user.Id,
						Username = user.UserName,
						FirstName = user.FirstName,
						LastName = user.LastName
					};

					await dbContext.Users.AddAsync(toDoUser);
					await dbContext.SaveChangesAsync();

					return RedirectToAction("login");
				}
				foreach (IdentityError error in result.Errors)
				{
					ModelState.AddModelError("", error.Description);
				}
			}

			return View(model);
		}

		[HttpGet]
		public IActionResult Login([FromQuery] string returnUrl = null)
		{
			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Profile");
			}

			LoginModel model = new LoginModel()
			{
				ReturnUrl = returnUrl
			};
			return View(model);
		}

		[HttpPost]
		public async Task<IActionResult> Login(LoginModel model)
		{
			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Profile");
			}

			if (ModelState.IsValid)
			{
				Microsoft.AspNetCore.Identity.SignInResult result = await signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);
				if (result.Succeeded)
				{
					return LocalRedirect(model.ReturnUrl ?? "/");
				}
				ModelState.AddModelError("", "Invalid username or password");
			}
			return View(model);
		}

		[HttpGet]
		[Authorize]
		public async Task<IActionResult> Logout()
		{
			await signInManager.SignOutAsync();

			return RedirectToAction("login");
		}

		[HttpGet]
		[Authorize]
		public IActionResult Profile()
		{
			// TODO: Implement Profile's view
			return View();
		}
	}
}
