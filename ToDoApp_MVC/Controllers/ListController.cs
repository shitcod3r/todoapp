﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.DomainLibrary;
using ToDoApp_MVC.Models;
using ToDoApp_MVC.Models.ViewModel;

namespace ToDoApp_MVC.Controllers
{
	[Authorize]
	[Route("/[controller]/[action]")]
	public class ListController : Controller
	{
		private readonly IToDoRepository repository;

		public ListController(IToDoRepository repo)
		{
			repository = repo;
		}

		[HttpGet]
		public IActionResult New()
		{
			ViewData["Title"] = "New list";
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> New(ToDoListViewModel model)
		{
			ViewData["Title"] = "New list";

			if (ModelState.IsValid)
			{
				ToDoUser currentUser = await repository.GetUserAsync(this.User);

				if (currentUser is null)
				{
					return StatusCode(StatusCodes.Status500InternalServerError);
				}

				ToDoList toDoList = new ToDoList()
				{
					Title = model.Title,
					Description = model.Description,
					CreationTime = DateTime.Now,
					Author = currentUser
				};

				await repository.AddAsync(toDoList);

				return RedirectToAction(actionName: "Index", controllerName: "Home");
			}

			return View(model);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Edit(int id)
		{
			ViewData["Title"] = "Edit list";
			ViewBag.IsEdit = true;

			ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

			if (list is null)
			{
				return NotFound();
			}

			ToDoListViewModel model = new ToDoListViewModel()
			{
				Title = list.Title,
				Description = list.Description
			};

			return View("New", model);
		}

		[HttpPost("{id}")]
		public async Task<IActionResult> Edit(int id, [FromForm] ToDoListViewModel listModel)
		{
			ViewData["Title"] = "Edit list";

			if (ModelState.IsValid)
			{
				ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

				if (list is null)
				{
					return NotFound();
				}

				list.Title = listModel.Title;
				list.Description = listModel.Description;

				await repository.UpdateAsync(list);

				return RedirectToAction(actionName: "Index", controllerName: "Home");
			}

			return View("New", listModel);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			ViewData["Title"] = "Delete list";

			ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

			if (list is null) { return NotFound(); }

			return View(list);
		}

		[HttpPost("{id}")]
		public async Task<IActionResult> Delete(int id, [FromForm] int listId)
		{
			if (id != listId)
			{
				return BadRequest();
			}

			ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

			if (list is null)
			{
				return NotFound();
			}

			await repository.DeleteAsync(list);

			return RedirectToAction(actionName: "Index", controllerName: "Home");
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Hide(int id)
		{
			ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

			if (list is null)
			{
				return NotFound();
			}

			list.Hidden = true;
			await repository.SaveChangesAsync();

			return RedirectToAction(actionName: "Index", controllerName: "Home");
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> UnHide(int id)
		{
			ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

			if (list is null)
			{
				return NotFound();
			}

			list.Hidden = false;
			await repository.SaveChangesAsync();

			return RedirectToAction(actionName: "Index", controllerName: "Home");
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Copy(int id)
		{
			ViewData["Title"] = "Copy list";
			ViewBag.IsCopy = true;

			ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

			if (list is null)
			{
				return NotFound();
			}

			ToDoListViewModel model = new ToDoListViewModel()
			{
				Title = list.Title,
				Description = list.Description
			};

			return View("New", model);
		}

		[HttpPost("{id}")]
		public async Task<IActionResult> Copy(int id, ToDoListViewModel model)
		{
			if (ModelState.IsValid)
			{
				ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id, includeEntries: true);

				if (list is null)
				{
					return NotFound();
				}

				ToDoList newList = new ToDoList()
				{
					Title = model.Title,
					Description = model.Description,
					CreationTime = DateTime.Now,
					Author = list.Author,
					Hidden = list.Hidden
				};

				repository.Update(newList);

				foreach (var task in list.Entries)
				{
					newList.Entries.Add(new ToDoEntry
					{
						Title = task.Title,
						Description = task.Description,
						CreationTime = DateTime.Now,
						HasDueTime = task.HasDueTime,
						DueTime = task.DueTime,
						Progress = task.Progress,
						ToDoListId = newList.Id
					});
				}

				await repository.SaveChangesAsync();

				return RedirectToAction(actionName: "Index", controllerName: "Home");
			}

			return View("New", model);
		}
	}
}
