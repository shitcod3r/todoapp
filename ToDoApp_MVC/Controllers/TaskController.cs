﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.DomainLibrary;
using ToDoApp_MVC.Models;
using ToDoApp_MVC.Models.ViewModel;

namespace ToDoApp_MVC.Controllers
{
	[Authorize]
	[Route("/[controller]/[action]")]
	public class TaskController : Controller
	{
		private readonly IToDoRepository repository;

		public TaskController(IToDoRepository repo)
		{
			repository = repo;
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> New(int id)
		{
			ViewData["Title"] = "Create Task";

			ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

			if (list == null)
			{
				return BadRequest();
			}

			ToDoEntryViewModel model = new ToDoEntryViewModel();
			model.DueTime = DateTime.Now.AddDays(1);

			return View(model);
		}

		[HttpPost("{id}")]
		public async Task<IActionResult> New(int id, ToDoEntryViewModel model)
		{
			ViewData["Title"] = "Create Task";

			if (ModelState.IsValid)
			{
				ToDoList list = await repository.GetUserToDoListByIdAsync(this.User, id);

				if (list == null)
				{
					return BadRequest();
				}

				ToDoEntry entry = new ToDoEntry()
				{
					Title = model.Title,
					Description = model.Description,
					HasDueTime = model.HasDueTime,
					ToDoListId = list.Id
				};
				if (model.HasDueTime)
					entry.DueTime = model.DueTime;

				list.Entries.Add(entry);

				await repository.UpdateAsync(list);

				return RedirectToAction(actionName: "Index", controllerName: "Home");
			}

			return View(model);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Edit(int id)
		{
			ViewData["Title"] = "Edit Task";
			ViewBag.IsEdit = true;

			ToDoEntry task = await repository.GetUserToDoEntryByIdAsync(this.User, id);

			if (task is null)
			{
				return NotFound();
			}

			ToDoEntryViewModel model = new ToDoEntryViewModel()
			{
				Title = task.Title,
				Description = task.Description,
				HasDueTime = task.HasDueTime
			};
			if (task.HasDueTime)
				model.DueTime = task.DueTime;

			return View("New", model);
		}

		[HttpPost("{id}")]
		public async Task<IActionResult> Edit(int id, [FromForm] ToDoEntryViewModel model)
		{
			ViewData["Title"] = "Edit Task";
			ViewBag.IsEdit = true;

			ToDoEntry task = await repository.GetUserToDoEntryByIdAsync(this.User, id);

			if (ModelState.IsValid)
			{
				if (task is null)
				{
					return NotFound();
				}

				task.Title = model.Title;
				task.Description = model.Description;
				task.HasDueTime = model.HasDueTime;
				if (model.HasDueTime)
				{
					task.DueTime = model.DueTime;
				}

				await repository.UpdateAsync(task);

				return RedirectToAction(actionName: "Index", controllerName: "Home");
			}

			model.Title = task.Title;

			return View("New", model);
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			ToDoEntry task = await repository.GetUserToDoEntryByIdAsync(this.User, id);

			if (task is null)
			{
				return NotFound();
			}

			await repository.DeleteAsync(task);

			return RedirectToAction(actionName: "Index", controllerName: "Home");
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Progress(int id, ToDoProgress newProgress)
		{
			ToDoEntry task = await repository.GetUserToDoEntryByIdAsync(this.User, id);

			if (task is null)
			{
				return NotFound();
			}

			task.Progress = newProgress;
			await repository.UpdateAsync(task);

			return RedirectToAction(actionName: "Index", controllerName: "Home");
		}
	}
}
