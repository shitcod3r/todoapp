﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ToDoApp_MVC.Models;
using ToDoApp_MVC.Models.ViewModel;

namespace ToDoApp_MVC.Controllers
{
	[Authorize]
	public class HomeController : Controller
	{
		private readonly IToDoRepository repository;

		public HomeController(IToDoRepository repo)
		{
			repository = repo;
		}

		[HttpGet]
		[Route("/")]
		public IActionResult Index()
		{
			ViewData["Title"] = "Index";
			ViewBag.IsIndex = true;


			DateTime endOfTheDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
			DateTime endOfNext7Days = DateTime.Now.AddDays(7);

			var userLists = repository.GetUserToDoLists(this.User);

			var hiddenLists = userLists.Where(l => l.Hidden);
			var toDoLists = userLists.Except(hiddenLists);

			var tasksWithDueTime = toDoLists
				.SelectMany(l => l.Entries)
				.Where(t => t.HasDueTime)
				.OrderBy(t => t.Progress)
				.ThenBy(t => t.DueTime);

			IndexViewModel model = new IndexViewModel
			{
				ToDoLists = toDoLists,
				HiddenLists = hiddenLists,
				TasksForToday = tasksWithDueTime.Where(t => t.DueTime < endOfTheDay)
			};
			model.TasksForNext7Days = tasksWithDueTime.Except(model.TasksForToday).Where(t => t.DueTime < endOfNext7Days);

			return View(model);
		}
	}
}
