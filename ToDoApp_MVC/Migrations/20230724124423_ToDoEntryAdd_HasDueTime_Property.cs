﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoApp_MVC.Migrations
{
    public partial class ToDoEntryAdd_HasDueTime_Property : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasDueTime",
                table: "Tasks",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasDueTime",
                table: "Tasks");
        }
    }
}
