﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoApp_MVC.Migrations
{
    public partial class ToDoList_add_Hidden_flag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Hidden",
                table: "ToDoList",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Hidden",
                table: "ToDoList");
        }
    }
}
