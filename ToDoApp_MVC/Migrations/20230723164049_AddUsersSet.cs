﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoApp_MVC.Migrations
{
    public partial class AddUsersSet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ToDoEntry_ToDoList_ToDoListId",
                table: "ToDoEntry");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ToDoEntry",
                table: "ToDoEntry");

            migrationBuilder.RenameTable(
                name: "ToDoEntry",
                newName: "Tasks");

            migrationBuilder.RenameIndex(
                name: "IX_ToDoEntry_ToDoListId",
                table: "Tasks",
                newName: "IX_Tasks_ToDoListId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Tasks",
                table: "Tasks",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_ToDoList_ToDoListId",
                table: "Tasks",
                column: "ToDoListId",
                principalTable: "ToDoList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tasks_ToDoList_ToDoListId",
                table: "Tasks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Tasks",
                table: "Tasks");

            migrationBuilder.RenameTable(
                name: "Tasks",
                newName: "ToDoEntry");

            migrationBuilder.RenameIndex(
                name: "IX_Tasks_ToDoListId",
                table: "ToDoEntry",
                newName: "IX_ToDoEntry_ToDoListId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ToDoEntry",
                table: "ToDoEntry",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ToDoEntry_ToDoList_ToDoListId",
                table: "ToDoEntry",
                column: "ToDoListId",
                principalTable: "ToDoList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
