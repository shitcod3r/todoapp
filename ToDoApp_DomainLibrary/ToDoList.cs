﻿using System;
using System.Collections.Generic;

namespace ToDoApp.DomainLibrary
{
	public class ToDoList
	{
		public int Id { get; set; }
		public string Title { get; set; } = string.Empty;
		public string Description { get; set; } = string.Empty;
		public DateTime CreationTime { get; set; } = DateTime.Now;
		public List<ToDoEntry> Entries { get; set; } = new List<ToDoEntry>();
		public ToDoUser Author { get; set; }
		public bool Hidden { get; set; } = false;
	}
}
