﻿namespace ToDoApp.DomainLibrary
{
    public enum ToDoProgress
    {
        NotStarted,
        InProgress,
        Completed
    }
}
