﻿using System;

namespace ToDoApp.DomainLibrary
{
	public class ToDoEntry
	{
		public int Id { get; set; }
		public string Title { get; set; } = string.Empty;
		public string Description { get; set; } = string.Empty;
		public bool HasDueTime { get; set; } = false;
		public DateTime DueTime { get; set; }
		public DateTime CreationTime { get; set; } = DateTime.Now;
		public ToDoProgress Progress { get; set; } = ToDoProgress.NotStarted;
		public int ToDoListId { get; set; }
	}
}
